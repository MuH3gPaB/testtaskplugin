package my.testtask.dao;

import com.atlassian.sal.api.transaction.TransactionCallback;
import my.testtask.model.Celebration;
import my.testtask.model.CelebrationEntity;
import net.java.ao.Query;

import java.util.ArrayList;
import java.util.List;

public class CelebrationDAOImpl implements CelebrationDAO {
    public List<Celebration> getAll() {
        final List<Celebration> result = new ArrayList<Celebration>();
        LaboratoryDAO.getAo().executeInTransaction(new TransactionCallback<Void>() {
            public Void doInTransaction() {
                for (CelebrationEntity celeb : LaboratoryDAO.getAo().find(CelebrationEntity.class)) {
                    result.add(new Celebration(celeb));
                }
                return null;
            }
        });
        return result;
    }

    public Celebration update(long id, String name, String date) {
        CelebrationEntity celeb = LaboratoryDAO.getAo().executeInTransaction(new TransactionCallback<CelebrationEntity>() {
            public CelebrationEntity doInTransaction() {
                CelebrationEntity celeb = LaboratoryDAO.getAo().find(CelebrationEntity.class, Query.select().where("ID=?", id))[0];
                if (date != null) celeb.setDate(date);
                if (name != null) celeb.setName(name);
                celeb.save();
                return celeb;
            }
        });
        return new Celebration(celeb);
    }

    public Celebration add(String name, String date) {
        CelebrationEntity celeb = LaboratoryDAO.getAo().executeInTransaction(new TransactionCallback<CelebrationEntity>() {
            public CelebrationEntity doInTransaction() {
                final CelebrationEntity celeb = LaboratoryDAO.getAo().create(CelebrationEntity.class);
                if(date != null) celeb.setDate(date);
                if(name != null) celeb.setName(name);
                celeb.save();
                return celeb;
            }
        });
        return new Celebration(celeb);
    }

    public Celebration delete(long id) {
        CelebrationEntity celeb = LaboratoryDAO.getAo().executeInTransaction(new TransactionCallback<CelebrationEntity>() {
            public CelebrationEntity doInTransaction() {
                CelebrationEntity celeb = LaboratoryDAO.getAo().find(CelebrationEntity.class, Query.select().where("ID=?", id))[0];
                LaboratoryDAO.getAo().delete(celeb);
                return celeb;
            }
        });
        return new Celebration(celeb);
    }
}
