package my.testtask.dao;

import com.atlassian.activeobjects.external.ActiveObjects;

public class LaboratoryDAO {

    private static ActiveObjects ao;
    private static LaboratoryDAO instance = null;

    public static ActiveObjects getAo() {
        if (instance == null) {
            synchronized (LaboratoryDAO.class) {
                if (instance == null) {
                    instance = new LaboratoryDAO();
                }
            }
        }
        return ao;
    }

    private LaboratoryDAO() {

    }

    public LaboratoryDAO(ActiveObjects ao) {
        LaboratoryDAO.ao = ao;
    }
}
