package my.testtask.dao;

import my.testtask.model.Celebration;
import my.testtask.model.CelebrationEntity;

import java.util.List;

public interface CelebrationDAO {
    List<Celebration> getAll();
    Celebration update(long id, String name, String date);
    Celebration add(String name, String date);
    Celebration delete(long id);
}
