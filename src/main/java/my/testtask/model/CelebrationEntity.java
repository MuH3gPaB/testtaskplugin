package my.testtask.model;

import net.java.ao.Preload;
import net.java.ao.Entity;

@Preload
public interface CelebrationEntity extends Entity {
    public String getName();

    public void setName(String name);

    public String getDate();

    public void setDate(String date);
}
