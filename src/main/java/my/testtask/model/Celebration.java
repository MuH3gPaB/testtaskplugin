package my.testtask.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Celebration {

    @XmlElement
    private long id;

    @XmlElement
    private String name;

    @XmlElement
    private String date;

    private Celebration() {
    }

    public Celebration(String name, String date) {
        this.name = name;
        this.date = date;
    }

    public Celebration(long id, String name, String date) {
        this.id = id;
        this.name = name;
        this.date = date;
    }

    public Celebration(CelebrationEntity celeb) {
        this.id = celeb.getID();
        this.name = celeb.getName();
        this.date = celeb.getDate();
    }
}
