package my.testtask.rest;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.transaction.TransactionCallback;
import my.testtask.dao.CelebrationDAO;
import my.testtask.dao.LaboratoryDAO;
import my.testtask.model.Celebration;
import my.testtask.model.CelebrationEntity;
import net.java.ao.Query;

import java.util.ArrayList;
import java.util.List;

@Path("laboratory")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class LaboratoryRestTable {

    CelebrationDAO celebrationDAO;

    @GET
    @Path("all")
    @AnonymousAllowed
    public Response getAll() {
        return Response.ok(celebrationDAO.getAll()).build();
    }

    @PUT
    @Path("{id}")
    public Response updateVersion(@PathParam("id") final String id, final String data) throws JSONException {
        final long idLong = Long.parseLong(id);
        JSONObject jsonObject = new JSONObject(data.substring(data.indexOf("{")));
        final String name, date;
        if(data.contains("name")) name = jsonObject.getString("name");
        else name = null;
        if(data.contains("date")) date = jsonObject.getString("date");
        else date = null;
        return Response.ok(celebrationDAO.update(idLong, name, date)).build();
    }


    @POST
    public Response addCelebration(final String data) throws JSONException {
        JSONObject jsonObject = new JSONObject(data.substring(data.indexOf("{")));
        final String name, date;
        if(data.contains("name")) name = jsonObject.getString("name");
        else name = "Default name";
        if(data.contains("date")) date = jsonObject.getString("date");
        else date = null;

        return Response.ok(celebrationDAO.add(name, date)).build();
    }

    @DELETE
    @Path("{id}")
    public Response delete(@PathParam("id") final String id) {
        final long idLong = Long.parseLong(id);
        return Response.ok(celebrationDAO.delete(idLong)).build();
    }

    public LaboratoryRestTable(CelebrationDAO celebrationDAO) {
        this.celebrationDAO = celebrationDAO;
    }
}