jQuery(function () {

    LaboratoryTable = new AJS.RestfulTable({
        autoFocus: true,
        el: jQuery("#project-laboratory-table"),
        allowReorder: true,
        resources: {
            all: contextPath + "/rest/laboratory/1.0/laboratory/all",
            self: contextPath + "/rest/laboratory/1.0/laboratory"
        },
        columns: [
            {
                id: "name",
                header: "Celebration"
            },
            {
                id: "date",
                header: "Date",
                createView: AJS.RestfulTable.CustomCreateView.extend({
                    render: function () {
                        var el = this.$el;
                        el.html('<input class="aui-date-picker" name="date" type="date" max="2030-01-01" min="2010-01-01" />');
                        return el;
                    }
                }),
                editView: AJS.RestfulTable.CustomEditView.extend({
                    render: function () {
                        var date = this.model.attributes.date;
                        var el = this.$el;
                        el.html('<input class="aui-date-picker" name="date" type="date" max="2030-01-01" min="2010-01-01" value="'+date+'"/>');
                        return el;
                    }
                })
            }
        ]
    });
});



